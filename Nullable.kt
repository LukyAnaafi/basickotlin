fun main(){
    //harus menggunakan ? setelah tipe data
     var firstName : String? = "Joko"
    firstName = null
    //cara mengaksesnya 
    println(firstName?.length)
    //cara mengakses yang SALAH 
    println(firstName.length)
}