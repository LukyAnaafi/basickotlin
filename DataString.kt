fun main(){
    var kalimatOne : String = "Jogjakarta"
    var kalimatTwo : String = "Istemewa"

    println(kalimatOne)
    println(kalimatTwo)

    //Kode String Margin
    var addres : String ="""
        |Jogjakarta
        |Itu
        |Istemewa
    """.trimMargin()

    println(addres)

    //Menggabungkan String
    var namaDepan : String = "Sergio"
    var namaBelakang : String = "Messi"

    val namaLengkap : String = namaDepan +" "+ namaBelakang

    println(namaLengkap)

    //Menggunakan String Template
    var namaDepan : String = "Fabregas"
    var namaBelakang : String = "Darmono"

    val fullName : String ="$namaDepan $namaBelakang"
    var desc : String ="Nama dia adalah $fullName dia merupakan gelandang terbaik"



    /*output :
    Jogjakarta
    Istemewa

    Jogjakarta
    Itu
    Istemewa

    Sergio Messi
    
    Nama dia adalah Fabregas Darmono dia merupakan gelandang terbaik

     */
}