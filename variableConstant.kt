/*
    Constant adalah Immutable data, yang biasanya diakses untuk keperluan global.
Global artinya bisa diakses dimanapun
Untuk menandai bahwa variable tersebut adalah constant, biasanya menggunakan UPPER_CASE dalam pembuatan nama variable constant nya

 */
 const val APP : String = "Youtube"
 const val VERSION : String = "Premium"

 fun main(){
    println("Selamat anda telah berlangganan $APP versi $VERSION")
    //outout : Selamat anda telah berlangganan Youtube versi Premium
 }