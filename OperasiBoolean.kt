fun main(){
	var nilaiRapor = 80
    var nilaiEkstra = 79
    
    val lulusNilaiRapor = nilaiRapor > 78
    val lulusNilaiEkstra = nilaiEkstra > 75
    
    val apakahLulus = lulusNilaiRapor && lulusNilaiEkstra
    println(apakahLulus)//output : true
}