fun main(){
    val peserta : Array<String> = arrayOf("Metallica","Iron Maiden","Guns N Roses")
    
    println(peserta.get(0))
    peserta.set(0,"KangenBand")
    val KangenBand : String = peserta[0]
    println(peserta[0])

    //sebelum diganti output :
    //Metallica

    //setelah diganti output :
    //KangenBand
    
     /*
     size -> Untuk mendapatkan panjang Array
    get(index) -> Mendapat data di posisi index
    [index]
    Mendapat data di posisi index
    set(index, value) -> Mengubah data di posisi index
    [index] = value -> Mengubah data di posisi index
     */
    //Array Nullable
    val name : Array<String?> = arrayOfNulls(5)
    name.set(0,"Kopi")
    name.set(1,"Teh")
    name.set(2,"Jus")
    name.set(3,"Milo")
    name.set(4,"Jeruk")
    println(name.size)
    //output : 5
}