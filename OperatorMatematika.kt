     fun main(){
     var jawaban = 10 + 5
    println(jawaban)//output : 15
    
    var total: Int = 0
    
    val barang1 = 20_000
    total+= barang1 //contoh penggunaan Augmented Assignments

    val barang2 = 50_000 
    total+= barang2 //contoh penggunaan Augmented Assignments
    
    val barang3 = 10_000
    total = total + barang3  //TANPA Augmented Assignments
    
    val barang4 = 50_000
    total-= barang4 //contoh penggunaan Augmented Assignments
    
    println(total)//output : 30000
    
    /*
    Augmented Assignments


    a = a + 10 -> a += 10
    a = a - 10 -> a -= 10
    a = a * 10 -> a *= 10
    a = a / 10 -> a /= 10
    a = a % 10 -> a %= 10

     */
}
