/*Kotlin mendukung 2 jenis variabel; Mutable (bisa diubah) dan Immutable (tidak bisa diubah).
Untuk membuat variable Mutable, di kotlin bisa menggunakan kata kunci var
Untuk membuat variable Immutable, di kotlin bisa menggunakan kata kunci val
 */
 //Deklarasi Variable
 //val/var namaVariable : TipeData = data
 fun main(){
    //contoh variable mutable
    var namaAwal: String = "Ronaldo"
    var noPunggung: Int = 9
    println("$namaAwal bernomor punngung $noPunggung")
    //output : Ronaldo bernomor punngung 9
    noPunggung = 7
    println("$namaAwal bernomor punngung $noPunggung")
    //output : Ronaldo bernomor punngung 7

    //contoh variable Immutable
    val name : String = "Messi"
    val number : Int = 30
    val profil ="$name nomor $number"
    println(profil)
    number = 10
    println(profil)
    //output : Val cannot be reassigned

 }