//When Expression itu seperti If tapi lebih
fun main(){
    val nilai = 'A'
    var nilaiUjian = 'A'
    
    when(nilai){
        'A'->{
	    println("Sangat Baik")
        }
        'B'->{
            println("Bagus")
        }
        'C'->{
            println("Lumayan Baik")
        }
        'D' ->{
            println("Cukup")
        }
        else ->{
            println("Ditingkatkan Lagi") //Output : Sangat Baik
        }
    }
    
    //When Expression Multiple Option
    when(nilaiUjian){
        'A','B','C'-> println ("Lulus")
        else ->{
            println("Belajarlagi")
        }
    }//Output : Lulus
    nilaiUjian = 'Z'
    val nilaiLulus= arrayOf('A','B','C')
    when (nilaiUjian){
        in nilaiLulus -> println("Selamat Anda Lulus")
        !in nilaiLulus -> println("Anda kurang beruntung")//Output : Anda Kurang Beruntung
    }
}